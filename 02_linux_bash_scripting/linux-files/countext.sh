#!/usr/bin/env bash
ext=$1
dir=$2

cd $dir
ls -R | grep -P "(\.${ext})$" | wc -l

