from random import seed
from random import randint

#seed(1)
random_num = randint(1, 100)
guess = int(input('Enter guess: '))
guesses = 20
iter = 0
diff = 0
win = False

while iter <= guesses:
    if guess == 0:
        print("Done playing")
        zero_exit = True
        break
    iter = iter + 1
    if abs(guess - random_num) < diff:
        print('warmer')
    elif abs(guess - random_num) > diff and iter != 1:
        print('colder')    
    diff = abs(guess - random_num)
    if diff == 0:
        print("You won!")
        win = True
        break
    if diff <= 5:
        print("Hot!")
    else:
        print("Cold")
    guess = int(input('Guess Again: '))    

if not win:
    if zero_exit:
        print("Player exited.")
    else:    
        print("Ran out of guesses.  Please try again")
