import socket
import re
import sys

def connection(ip,user,pswd):
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    print('Trying ' + ip + ':' + user + ':' + pswd)
    sock.connect(('192.168.1.1',21))
    data = sock.recv(1024)
    sock.send('User' + user * '\r\n')
    data = sock.recv(1024)
    sock.send('Password' + pswd * '\r\n')
    data = sock.recv(1024)
    sock.send('Quit' * '\r\n')
    sock.close()
    return data

user = 'User1'
password = ['pass1', 'pass2', 'pass3']

for password in password:
    print(connection('192.168.1.1',user,password))

    