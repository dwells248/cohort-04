board = {
    'slot1' : ' ', 'slot2' : ' ', 'slot3' : ' ',
    'slot4' : ' ', 'slot5' : ' ', 'slot6' : ' ',
    'slot7' : ' ', 'slot8' : ' ', 'slot9' : ' '
}

used_slots = []


def printline(character, length):
    print(character * length)

def printboard(board):
    for slots in board:
        print(board[slots], end = ' ')
        if slots not in ('slot3', 'slot6', 'slot9'):
            print('|', end = ' ')
        elif slots != 'slot9':
            print()
            printline('-', 9)
    print() 

def checkwin(board, letter):
    return ((board['slot1'] + board['slot2'] + board['slot3'] == letter * 3) or #across top
            (board['slot4'] + board['slot5'] + board['slot6'] == letter * 3) or #across middle
            (board['slot7'] + board['slot8'] + board['slot9'] == letter * 3) or #across bottom
            (board['slot1'] + board['slot5'] + board['slot9'] == letter * 3) or #diagonal 1
            (board['slot7'] + board['slot5'] + board['slot3'] == letter * 3) or #diagonal 2
            (board['slot1'] + board['slot4'] + board['slot7'] == letter * 3) or #down left
            (board['slot2'] + board['slot5'] + board['slot8'] == letter * 3) or #down middle
            (board['slot3'] + board['slot6'] + board['slot9'] == letter * 3))   #down right
           
                   

printboard(board)
while len(used_slots) < 9:
    slot = 'slot' + input('Select slot (1 - 9): ')
    if slot in used_slots:
        print('slot not available')
        continue
    board[slot] = 'X'
    if checkwin(board, 'X'):
        printboard(board)
        print('X wins!')
        break
else:
    print('Draw')    

    used_slots.append(slot)
    printboard(board)

       

