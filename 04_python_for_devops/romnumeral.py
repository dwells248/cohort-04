roman = {'M': 1000,
         'D': 500,
         'C': 100,
         'L': 50,
         'X': 10,
         'V': 5,
         'I': 1}

getroman = input('Enter roman numeral: ')

arabic = [roman[numeral] for numeral in getroman]

arabicsum = 0

for index in range(len(arabic)):
    try:
        if arabic[index] < arabic[index + 1]:
            arabic[index] = arabic[index] * -1
    except IndexError:
        arabicsum = arabicsum   
    arabicsum = arabicsum + arabic[index]

print(getroman, arabicsum)