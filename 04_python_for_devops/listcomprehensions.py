import math
colors = ['black', 'white']
sizes = ['S', 'M', 'L']
sleeves = ['short', 'long']

tshirts = [[color, size, sleeve] for color in colors
                                 for size in sizes
                                 for sleeve in sleeves]

print(tshirts)

squares = [number ** 2 for number in range(1,26)]

print(squares)

words = ['hello', 'bye', 'world', 'banana', 'apple', 'cloud', 'cherry']
vowels = ['a', 'e', 'i', 'o', 'u']
novowel = [word for word in words
                if word[-1] not in vowels]

print(novowel)

notdivby5 = [number for number in range(1,100)
                    if number % 5]

print(notdivby5)