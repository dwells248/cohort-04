import re

wordcount_dict = {}

filename = input('Enter file path: ')

with open(filename) as filein:
    fileinlist = filein.readlines()

for line in fileinlist:
    words = line.strip().split()
    for word in words:
        countword = re.search('(([a-zA-Z])+-([a-zA-Z])+)|([a-zA-Z]+)', word)
        if countword:
            if wordcount_dict.get(countword.group(0).lower()):
                wordcount_dict[countword.group(0).lower()] = wordcount_dict[countword.group(0).lower()] + 1
            else:
                wordcount_dict[countword.group(0).lower()] = 1

print(wordcount_dict)





