list1 = []
list2 = []
lists = [list1, list2]

def actionchoice():
    print('1. add to list')
    print('2. remove from list')
    print('3. reverse list')
    print('4. display list')
    print('5. compare lists')
    print('Q. Quit')
    choice = input('Enter action: ')
    return(choice)

def listchoice():
    print('1. list1')
    print('2. list2')
    choice = input('Enter list: ')
    listchoice = 'list' + choice
    return(choice)

def removetypechoice():
    print('1. Index')
    print('2. Value')
    choice = input('Enter remove type: ')
    return(choice)    

def reverselist(list):
    list.sort(reverse=True)

def addlist(list, item):
    list.append(item)

def removelist(list, removetype, item):
    if removetype == '1':
        del list[int(item)]
    else:
        list.remove(item) 

def enteritem(list1, list2):
    displaylists(list1, list2)
    item = input('Enter item or index: ')
    return(item)

def displaylists(list1, list2):
    print('List1: ', list1)
    print('List2: ', list2)

def comparelists(list1, list2):
    list1_sorted = sorted(list1)
    list2_sorted = sorted(list2)
    return(list1_sorted == list2_sorted)


while True:
    action = actionchoice()
    if action.upper() == 'Q':
        break
    elif action == '1':
        addlist(lists[listchoice()], enteritem(list1, list2))
        # if listchoice() == 'list1':
        #     addlist(list1, enteritem(list1, list2))
        # else:
        #     addlist(list2, enteritem(list1, list2))    
    elif action == '2':
        if listchoice() == 'list1':
            removelist(list1, removetypechoice(), enteritem(list1, list2))
        else:
            removelist(list2, removetypechoice(), enteritem(list1, list2))     
    elif action == '3':
        if listchoice() == 'list1':
            reverselist(list1)
        else:
            reverselist(list2)  
    elif action == '4':
        displaylists(list1, list2)
    elif action == '5':
        if comparelists(list1, list2):
            print('The two lists are the same')
        else:
            print('The two lists are different')    



