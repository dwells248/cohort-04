# wordlist file taken from  https://theworld.com/%7Ereinhold/diceware.wordlist.asc

import secrets
import hashlib
wordlist = []
special = ['!', '@', '#', '%', '^', '&', '*']
number = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

with open('wordlist.txt', 'r') as file:
    for line in file:
        index, word = line.strip().split()
        wordlist.append(word)

def passgen():
    passwd = ''
    for i in range(1, number_of_words + 1):
        passwd = passwd + secrets.choice(wordlist).title() + secrets.choice(special)
    passwd = passwd + secrets.choice(number)    
    return(passwd)

number_of_words = int(input('Enter number of words in your password: '))

while True:
    passwd = passgen()
    if len(passwd) >= 15:
        break

print(passwd)

with open('md5hash.txt', 'w') as fileout:
    enc_passwd = passwd.encode('utf8')
    fileout.write(hashlib.md5(enc_passwd.strip()).hexdigest())