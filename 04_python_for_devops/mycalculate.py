import operator
import math
import sys


def calc(op1, op2, oper):
    """ *** calc function help ***

    Operators:
    log Operands must be non zero positive integers.  Operand 2 is log base.
    /   Operand 2 can't be zero (i.e 4 0 /) 
    +   Operands must be numeric positive or negative
    -   Operands must be numeric positive or negative
    *   Operands must be numeric positive or negative.  Need to enclose this in quotes or preceed with backslash.
    """
    calc_dict = {'+': operator.add,
                 '-': operator.sub,
                 '*': operator.mul,
                 '/': operator.truediv,
                 'log': math.log,
                 }            
    try:
        result = calc_dict[oper](int(op1), int(op2))
    except KeyError:
        return 'ERR101:Valid operators are + - * / or log'    
    except ValueError:
        return 'ERR201:Operands not valid for operator.  Pass ? ? ? to see function help.'    
    except ZeroDivisionError:
        return 'ERR301:Cant divide by zero'
    else:      
        return result
