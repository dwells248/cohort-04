#! /usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY="d21b17405d692b8977dd9098c59754eb"


def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
     """
    r = requests.get('https://ipvigilante.com')
    loc = r.json()
    return loc['data']['longitude'], loc['data']['latitude'], loc['data']['subdivision_1_name'], loc['data']['city_name']
    
def get_weather():
    """ Returns all weather data from dark sky for current location in json format"""
    lon = get_location()[0]
    lat = get_location()[1]
    url = 'https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY +'/' + lat +',' + lon
    r = requests.get(url)
    weather = r.json()
    return weather
    

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    r = get_weather()
    return r['currently']['temperature']

def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("Today's forecast is: ", temp)


if __name__ == "__main__":
    longitude = get_location()[0]
    latitude = get_location()[1]
    state = get_location()[2]
    city = get_location()[3]
    temp = get_temperature(longitude, latitude)
    print_forecast(temp)
    print(get_weather())

